module.exports =  {

    devServer: {
        // development server port 8000
        port: 8000,
        // If you want to turn on the proxy, please remove the mockjs /src/main.jsL11
        proxy: {
            [process.env.VUE_APP_API_BASE_URL]: {
                target: `http://127.0.0.1:8080`,
                changeOrigin: true,
                pathRewrite: {
                    ['^' + process.env.VUE_APP_API_BASE_URL]: ''
                }
            },
        }
    },
    configureWebpack:{

    }

}
