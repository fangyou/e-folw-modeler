# e-flow-ui

基于bpmn.js的flowable流程引擎的模型编辑器(Demo版)
保存按钮目前是输出xml至控制台
```
│  App.vue
│  main.js
│
├─assets
│      activiti.json
│      flowable.json #flowable 引擎参数类型绑定
│      logo.png
│
├─components
│  │  HelloWorld.vue #主页面
│  │
│  ├─customModeler #自定义bpmn.js
│  │  │  index.js
│  │  │
│  │  └─custom 
│  │          CustomContextPadProvider.js
│  │          CustomPalette.js
│  │          index.js
│  │
│  ├─Properties 右侧面板属性
│  │  │  bpmnHelper.js
│  │  │  mixins.js
│  │  │  ProcessProperties.vue
│  │  │  SequenceFlowProperties.vue
│  │  │  UserTaskProperties.vue
│  │  │
│  │  └─common
│  │          FieldTable.vue
│  │          ListenerTable.vue
│  │
│  └─Tool #功能按钮
│          Tool.vue
│
├─config
│      defaultXml.js #默认流程图
│      node.js #可使用的节点类型
│
├─i18n
│  └─customTranslate
│          customTranslate.js
│          translations.js #本地化文件
│
└─utils
```

## Project setup
```
yarn install

```

## 本地开发
```
yarn run prepare
yarn link
//子项目执行
yarn link e-flow-ui

```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```


