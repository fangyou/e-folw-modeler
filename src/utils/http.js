import request from '@/utils/request'

const http = {
  get (url, data, headers, responseType) {
    return request({
      url: url,
      method: 'GET',
      responseType,
      params: data,
      headers
    })
  },
  post (url, data, headers, responseType) {
    return request({
      url: url,
      method: 'POST',
      responseType,
      data: data,
      headers
    })
  },
  put (url, data, headers) {
    return request({
      url: url,
      method: 'PUT',
      data: data,
      headers
    })
  },
  delete (url, data, headers) {
    return request({
      url: url,
      method: 'DELETE',
      params: data,
      headers
    })
  }
}


export default http
