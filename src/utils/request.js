import axios from 'axios'
import { Notification } from 'element-ui'
import { VueAxios } from './axios'

// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 15000 // 请求超时时间
})

// 异常拦截处理器
const errorHandler = (error) => {
  if (error.response) {
    const data = error.response.data
    Notification.error({ title: '系统错误', message: data.message || 'Error', duration: 4000 ,offset:35})
    return Promise.reject(data)
  }else if(error.code==='ECONNABORTED'){
    Notification.error({ title: '连接超时', message: '请求失败,请重试', duration: 4000 ,offset:35})
  }
  return Promise.reject(error)
}

// request interceptor
request.interceptors.request.use(config => {

  return config
}, errorHandler)

// response interceptor
request.interceptors.response.use((response) => {
  return response
}, errorHandler)

const installer = {
  vm: {},
  install (Vue) {
    Vue.use(VueAxios, request)
  }
}

export default request

export {
  installer as VueAxios,
  request as axios
}
