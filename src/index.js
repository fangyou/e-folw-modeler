import BpmModel from './components/BpmModel'
 import BpmView from './components/BpmView'
 import Const from './components//Properties/common/Const'
// import CommonApproval from './components/CommonApproval'
const components = [
    BpmModel,
    BpmView,
]


const install = function(Vue, opts = {}) {
    components.forEach(component => {
        Vue.component(component.name, component)
    })
}
/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}

export default {
    install,
    BpmModel,
    Const,
    BpmView
}
