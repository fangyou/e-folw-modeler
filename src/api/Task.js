import http from '@/utils/http'
const url = '/business/eflow/task'


export function nextTaskNode (data) {
    return http.post(url + '/nextTaskNode', data)
}

export function complete (taskId,message,userList,variables) {
    return http.post(url + '/complete', {
        taskId,message,userList,variables
    })
}

export function getNextTaskNodeVoByTaskId (taskId,message,userList,variables) {
    return http.post(url + '/complete', {
        taskId,message,userList,variables
    })
}

export function getOneStepTaskNodeVoByProcessDefinitionId (processDefinitionId,variables) {
    return http.post(url + '/getOneStepTaskNodeVoByProcessDefinitionId', {
        processDefinitionId,variables
    })
}
export function getStartTaskNodeVoByProcessDefinitionKey (processDefinitionKey,variables) {
    return http.post(url + '/getOneStepTaskNodeVoByProcessDefinitionKey', {
        processDefinitionKey,variables
    })
}

export function getButtonList (taskId) {
    return http.get(url + '/getButtonList', {taskId})
}

export function getBackNodesByProcessInstanceId (data) {
    return http.get(url + '/getBackNodesByProcessInstanceId', data)
}

export function getProcessInstanceIdByBusinessId (businessId) {
    return http.get(url + '/getProcessInstanceIdByBusinessId/'+businessId)
}

export function jumpToSelectStep (processInstanceId,taskId,activityId,message,userList,variables) {
    return http.post(url + '/jumpToSelectStep', {
        processInstanceId,taskId,activityId,message,userList,variables
    })
}
