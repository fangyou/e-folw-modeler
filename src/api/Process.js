import http from '@/utils/http'
const url = '/business/eflow/process'
export function startProcessInstanceByKey (processDefinitionKey,userList,businessKey,title,variables) {
    return http.post(url + '/startProcessInstanceByKey', {
        processDefinitionKey,
        userList,
        businessKey,
        title,
        variables
    })
}
export function getProcessXmlByProcessInstanceId (processInstanceId) {
    return http.get(url + '/getProcessXmlByProcessInstanceId', {
        processInstanceId
    })
}
export function getPassPath (processInstanceId) {
    return http.get(url + '/getPassPath', {
        processInstanceId
    })
}

