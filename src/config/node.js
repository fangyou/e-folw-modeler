function createSubProcess(instance) {
    const {
        create,
        elementFactory,
        translate,
        spaceTool,
        lassoTool,
        handTool,
        globalConnect
    } = instance
   return  function createSubprocess(event) {
        var subProcess = elementFactory.createShape({
            type: 'bpmn:SubProcess',
            x: 0,
            y: 0,
            isExpanded: true
        })

        var startEvent = elementFactory.createShape({
            type: 'bpmn:StartEvent',
            x: 40,
            y: 82,
            parent: subProcess
        })

        create.start(event, [ subProcess, startEvent ], {
            hints: {
                autoSelect: [ startEvent ]
            }
        })
    }
}

export default {
    //开始节点
    'create.start-event': {
        group: 'start-event',
        type:'StartEvent',
        className: 'bpmn-icon-start-event-none',
        append:['append.user-task','append.end-event','append.exclusive-gateway','connect']
    },
    //用户任务
    'create.user-task': {
        group: 'activity',
        type:'UserTask',
        className: 'bpmn-icon-user-task',
        append:['append.user-task','append.end-event','append.exclusive-gateway','connect'],
    },
    //网关
    'create.exclusive-gateway': {
        group: 'gateway',
        type:'ExclusiveGateway',
        className: 'bpmn-icon-gateway-xor',
        append:['append.user-task','append.end-event','append.exclusive-gateway','connect']
    },
    //并行网关
    'create.parallel-gateway': {
        group: 'gateway',
        type:'ParallelGateway',
        className: 'bpmn-icon-gateway-parallel',
        append:['append.user-task','append.end-event','append.exclusive-gateway','connect']
    },
    //结束节点
    'create.end-event': {
        group: 'end-event',
        type:'EndEvent',
        className: 'bpmn-icon-end-event-none',
    },
    //子流程
    'create.subprocess-expanded': {
        group: 'activity',
        type: 'SubProcess',
        className: 'bpmn-icon-subprocess-expanded',
        title: 'SubProcess',
        append:['append.user-task','append.end-event','append.exclusive-gateway','connect'],
        action: {
            dragstart: createSubProcess,
            click: createSubProcess
        }
    },
}
