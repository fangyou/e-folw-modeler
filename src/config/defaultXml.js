export default `<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:flowable="http://flowable.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" targetNamespace="http://www.flowable.org/processdef">
  <process id="eflow_1" name="eflow_1">
    <startEvent id="Event_1kygs8s" flowable:initiator="initiator">
      <outgoing>Flow_1coe9lx</outgoing>
    </startEvent>
    <userTask id="Activity_1u2tqcr" name="发起人" flowable:assignee="\${userTaskService.getUser(execution)}">
      <extensionElements>
        <flowable:extended body="{&#34;setType&#34;:1,&#34;userList&#34;:[{&#34;id&#34;:1616489754518,&#34;type&#34;:5,&#34;data&#34;:[],&#34;_XID&#34;:&#34;row_5&#34;}]}" />
      </extensionElements>
      <incoming>Flow_1coe9lx</incoming>
      <outgoing>Flow_11atkmm</outgoing>
    </userTask>
    <sequenceFlow id="Flow_1coe9lx" sourceRef="Event_1kygs8s" targetRef="Activity_1u2tqcr" />
    <userTask id="Activity_075l69x" name="第一步" flowable:assignee="\${userTaskService.getUser(execution)}">
      <extensionElements>
        <flowable:extended body="{&#34;setType&#34;:2,&#34;userList&#34;:[{&#34;id&#34;:1616489762997,&#34;type&#34;:1,&#34;data&#34;:[{&#34;nickName&#34;:&#34;若依&#34;,&#34;userId&#34;:2,&#34;userName&#34;:&#34;ry&#34;},{&#34;nickName&#34;:&#34;若依&#34;,&#34;userId&#34;:1,&#34;userName&#34;:&#34;admin&#34;}],&#34;_XID&#34;:&#34;row_19&#34;}]}" />
      </extensionElements>
      <incoming>Flow_11atkmm</incoming>
      <outgoing>Flow_1as747q</outgoing>
    </userTask>
    <sequenceFlow id="Flow_11atkmm" sourceRef="Activity_1u2tqcr" targetRef="Activity_075l69x" />
    <userTask id="Activity_10vqu91" name="第二步" flowable:assignee="\${userTaskService.getUser(execution)}">
      <extensionElements>
        <flowable:extended body="{&#34;setType&#34;:2,&#34;userList&#34;:[{&#34;id&#34;:1616489774565,&#34;type&#34;:1,&#34;data&#34;:[{&#34;nickName&#34;:&#34;若依&#34;,&#34;userId&#34;:1,&#34;userName&#34;:&#34;admin&#34;}],&#34;_XID&#34;:&#34;row_33&#34;}]}" />
      </extensionElements>
      <incoming>Flow_1as747q</incoming>
      <outgoing>Flow_1mwj56o</outgoing>
    </userTask>
    <sequenceFlow id="Flow_1as747q" sourceRef="Activity_075l69x" targetRef="Activity_10vqu91" />
    <userTask id="Activity_0u0rj4f" name="第三步" flowable:assignee="\${userTaskService.getUser(execution)}">
      <extensionElements>
        <flowable:extended body="{&#34;setType&#34;:2,&#34;userList&#34;:[{&#34;id&#34;:1616489780798,&#34;type&#34;:1,&#34;data&#34;:[{&#34;nickName&#34;:&#34;若依&#34;,&#34;userId&#34;:2,&#34;userName&#34;:&#34;ry&#34;}],&#34;_XID&#34;:&#34;row_47&#34;}]}" />
      </extensionElements>
      <incoming>Flow_1mwj56o</incoming>
      <outgoing>Flow_14ax9rk</outgoing>
    </userTask>
    <sequenceFlow id="Flow_1mwj56o" sourceRef="Activity_10vqu91" targetRef="Activity_0u0rj4f" />
    <endEvent id="Event_0rdv9gj">
      <incoming>Flow_14ax9rk</incoming>
    </endEvent>
    <sequenceFlow id="Flow_14ax9rk" sourceRef="Activity_0u0rj4f" targetRef="Event_0rdv9gj" />
    <subProcess id="Activity_1eehenv">
      <extensionElements>
        <flowable:extended body="{&#34;setType&#34;:2,&#34;userList&#34;:[{&#34;id&#34;:1626331736859,&#34;type&#34;:4,&#34;data&#34;:{&#34;text&#34;:&#34;232323&#34;},&#34;_XID&#34;:&#34;row_5&#34;}]}" />
      </extensionElements>
      <startEvent id="Event_1d4ue8r" flowable:initiator="initiator" />
    </subProcess>
  </process>
  <bpmndi:BPMNDiagram id="BpmnDiagram_1">
    <bpmndi:BPMNPlane id="BpmnPlane_1" bpmnElement="eflow_1">
      <bpmndi:BPMNEdge id="Flow_14ax9rk_di" bpmnElement="Flow_14ax9rk">
        <omgdi:waypoint x="810" y="80" />
        <omgdi:waypoint x="872" y="80" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1mwj56o_di" bpmnElement="Flow_1mwj56o">
        <omgdi:waypoint x="650" y="80" />
        <omgdi:waypoint x="710" y="80" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1as747q_di" bpmnElement="Flow_1as747q">
        <omgdi:waypoint x="490" y="80" />
        <omgdi:waypoint x="550" y="80" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_11atkmm_di" bpmnElement="Flow_11atkmm">
        <omgdi:waypoint x="330" y="80" />
        <omgdi:waypoint x="390" y="80" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1coe9lx_di" bpmnElement="Flow_1coe9lx">
        <omgdi:waypoint x="178" y="80" />
        <omgdi:waypoint x="230" y="80" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Event_1kygs8s_di" bpmnElement="Event_1kygs8s">
        <omgdc:Bounds x="142" y="62" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1u2tqcr_di" bpmnElement="Activity_1u2tqcr">
        <omgdc:Bounds x="230" y="40" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_075l69x_di" bpmnElement="Activity_075l69x">
        <omgdc:Bounds x="390" y="40" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_10vqu91_di" bpmnElement="Activity_10vqu91">
        <omgdc:Bounds x="550" y="40" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_0u0rj4f_di" bpmnElement="Activity_0u0rj4f">
        <omgdc:Bounds x="710" y="40" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_0rdv9gj_di" bpmnElement="Event_0rdv9gj">
        <omgdc:Bounds x="872" y="62" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1eehenv_di" bpmnElement="Activity_1eehenv" isExpanded="true">
        <omgdc:Bounds x="220" y="170" width="350" height="200" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_1d4ue8r_di" bpmnElement="Event_1d4ue8r">
        <omgdc:Bounds x="260" y="252" width="36" height="36" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>`
