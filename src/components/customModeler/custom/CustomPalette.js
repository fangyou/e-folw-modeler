/**
 * A palette that allows you to create BPMN _and_ custom elements.
 */
import {
    assign
} from 'min-dash'
import nodeList from '@/config/node'
export default function PaletteProvider(palette, create, elementFactory,translate, globalConnect, bpmnFactory, spaceTool, lassoTool, handTool) {
    this.create = create
    this.elementFactory = elementFactory
    this.globalConnect = globalConnect
    this.translate = translate
    this.bpmnFactory = bpmnFactory
    this.spaceTool = spaceTool
    this.lassoTool = lassoTool
    this.handTool = handTool
    this.globalConnect = globalConnect
    palette.registerProvider(this)
}

PaletteProvider.$inject = [
    'palette',
    'create',
    'elementFactory',
    'translate',
    'globalConnect',
    'bpmnFactory',
    'spaceTool',
    'lassoTool',
    'handTool',
]

PaletteProvider.prototype.getPaletteEntries = function(element) {
    const {
        create,
        elementFactory,
        translate,
        spaceTool,
        lassoTool,
        handTool,
        globalConnect
    } = this
    const createAction = (type, group, className, title, options,action)=> {
        function createListener(event) {
            const shape = elementFactory.createShape(assign({ type:`bpmn:${type}`}, options))
            if (options) {
                shape.businessObject.di.isExpanded = options.isExpanded
            }
            create.start(event, shape)
        }
        const data = {
            group: group,
            className: className,
            title: title || (translate('Create') + translate(type)),
            action: {
                dragstart: createListener,
                click: createListener
            }
        }
        if(action){
            if(action.click){
                console.log('this',this)
                data.action.click = action.click(this)
            }
            if(action.dragstart){
                data.action.dragstart = action.dragstart(this)
            }
        }
        return data
    }


    const nodes = {
        'hand-tool': {
            group: 'tools',
            className: 'bpmn-icon-hand-tool',
            title: translate('Activate the hand tool'),
            action: {
                click: function(event) {
                    handTool.activateHand(event)
                }
            }
        },
        'lasso-tool': {
            group: 'tools',
            className: 'bpmn-icon-lasso-tool',
            title: translate('Activate the lasso tool'),
            action: {
                click: function(event) {
                    lassoTool.activateSelection(event)
                }
            }
        },
        'space-tool': {
            group: 'tools',
            className: 'bpmn-icon-space-tool',
            title: translate('Activate the create/remove space tool'),
            action: {
                click: function(event) {
                    spaceTool.activateSelection(event)
                }
            }
        },
        'global-connect-tool': {
            group: 'tools',
            className: 'bpmn-icon-connection-multi',
            title: translate('Activate the global connect tool'),
            action: {
                click: function(event) {
                    globalConnect.toggle(event)
                }
            }
        },
        'tool-separator': {
            group: 'tools',
            separator: true
        }
    }
    for(const key in nodeList){
        if (nodeList.hasOwnProperty(key)) {
            const node = nodeList[key]
            nodes[key] = createAction(node.type,node.group,node.className,node.title,node.options,node.action)
        }
    }
    return nodes
}
