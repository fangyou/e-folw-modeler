import {
    assign
} from 'min-dash'
import nodeList from '@/config/node'

const appendActions = {}
for (const key in nodeList) {
    if (nodeList.hasOwnProperty(key)&&nodeList[key].append) {
        appendActions[nodeList[key].type] = nodeList[key].append
    }
}
export default function ContextPadProvider(contextPad, config, injector, translate, bpmnFactory, elementFactory, create, modeling, connect) {
    this.create = create
    this.elementFactory = elementFactory
    this.translate = translate
    this.bpmnFactory = bpmnFactory
    this.modeling = modeling
    this.connect = connect
    config = config || {}
    if (config.autoPlace !== false) {
        this.autoPlace = injector.get('autoPlace', false)
    }
    contextPad.registerProvider(this)
}

ContextPadProvider.$inject = [
    'contextPad',
    'config',
    'injector',
    'translate',
    'bpmnFactory',
    'elementFactory',
    'create',
    'modeling',
    'connect'
]

ContextPadProvider.prototype.getContextPadEntries = function (element) {
    const {
        autoPlace,
        create,
        elementFactory,
        translate,
        connect,
        modeling
    } = this
    const businessObject = element.businessObject

    function startConnect(event, element) {
        connect.start(event, element)
    }

    function removeElement(e) {
        modeling.removeElements([element])
    }

    /**
     * Create an append action
     *
     * @param {string} type
     * @param {string} className
     * @param {string} [title]
     * @param {Object} [options]
     *
     * @return {Object} descriptor
     */
    function appendAction(type, className, title, options) {

        if (typeof title !== 'string') {
            options = title
            title = (translate('Append') + translate(type))
        }

        function appendStart(event, element) {

            var shape = elementFactory.createShape(assign({type: {type: `bpmn:${type}`}}, options))
            create.start(event, shape, {
                source: element
            })
        }


        var append = autoPlace ? function (event, element) {
            var shape = elementFactory.createShape(assign({type: `bpmn:${type}`}, options))

            autoPlace.append(element, shape)
        } : appendStart


        return {
            group: 'model',
            className: className,
            title: title,
            action: {
                dragstart: appendStart,
                click: append
            }
        }
    }

    const actions = {}
    const tempActions = {
        'append.user-task': appendAction(
            'UserTask',
            'bpmn-icon-user-task',
        ),
        'append.end-event': appendAction(
            'EndEvent',
            'bpmn-icon-end-event-none',
        ),
        'append.exclusive-gateway': appendAction(
            'ExclusiveGateway',
            'bpmn-icon-gateway-xor',
        ),
        'connect': {
            group: 'connect',
            className: 'bpmn-icon-connection-multi',
            title: translate('Connect using ' +
                (businessObject.isForCompensation ? '' : 'Sequence/MessageFlow or ') +
                'Association'),
            action: {
                click: startConnect,
                dragstart: startConnect
            }
        },
    }
    const type = businessObject.$type.replace(/^bpmn:/, '')
    //获取组件需要的附加组件
    for (const key in tempActions) {
        if (appendActions.hasOwnProperty(type)) {
            if (appendActions[type].indexOf(key) !== -1) {
                actions[key] = tempActions[key]
            }
        }
    }
    assign(actions, {
        'delete': {
            group: 'edit',
            className: 'bpmn-icon-trash',
            title: translate('Remove'),
            action: {
                click: removeElement
            }
        },
    })
    return actions
}
