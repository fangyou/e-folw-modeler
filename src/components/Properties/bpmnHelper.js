export default {
    inject: ['bpmnFactory'],
    methods:{
        createTaskListener(options){
           return this.bpmnFactory().create('flowable:TaskListener', options||{})
        },
        createExecutionListener(options){
           return this.bpmnFactory().create('flowable:ExecutionListener', options||{})
        },
        createField(options){
           return this.bpmnFactory().create('flowable:Field', options||{})
        }
    }
}
