export default {
    inject: ['bpmnModeler', 'modeling', 'bpmnFactory'],
    props: {
        element: {
            type: Object,
            required: true
        }
    },
    data(){
      return{
          definitions:{
              extensionElements:{
                  type:'bpmn:ExtensionElements',
                  field:'values',
              }
          }
      }
    },
    methods: {
        updateProperties(data){
            this.modeling().updateProperties(this.element,data)
        },
        /*初始化表单的值，此功能只对于绑定的element在当前组件创建之前就已经存在并且不会改变时使用
        * 如果element需要改变则需要重新调用init方法
        * */
        initFrom(form) {
            const businessObject = this.element.businessObject||this.element
            for (const key in form) {
                let value = businessObject.get(key)
                if(this.definitions.hasOwnProperty(key)&&value){
                    value = value.get(this.definitions[key].field)
                }
                if (value !== undefined) {
                    this.$set(form, key, value)
                }
            }
        },
        //监听值,实现流程图与表单的双向绑定
        watchFrom(index,form,model) {
            for (const key in form) {
                this.$watch(index+'.' + key, function (newVal, oldVal) {
                    const data = {}
                    data[key] = newVal
                    if(this.definitions.hasOwnProperty(key)){
                        //获取节点类型
                        const type = this.definitions[key].type
                        //组装数据
                        const attr = {}
                        attr[this.definitions[key].field] =  data[key]
                        //创建对象
                        data[key] = this.bpmnFactory().create(type, attr)
                    }
                    //删除表达式流程线上的属性也删掉
                    if(newVal===''){
                        data[key] = null
                    }
                    console.log(data)
                    if(model){
                        this.modeling().updateModdleProperties(this.element.$parent,this.element,data)
                    }else {
                        this.modeling().updateProperties(this.element,data)
                    }
                })
            }
        },
        init(key,form,model){
            this.initFrom(form)
            this.watchFrom(key,form,model)
        }
    },
    created() {

    }
}
